public package com.daacoworks.core.factory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;

public class ServerSocketFactory {
	
	
	/*
	 * ServerSocketFactory class to create a Server.
	 */
   
	
	public ServerSocketChannel createServer (ConnectionParameter cp) throws IOException{
	
		ServerSocketChannel serverSocketChannel=ServerSocketChannel.open();
		serverSocketChannel.bind(new InetSocketAddress(cp.getPort()));
		System.out.println("Server created successfully on port No:" +cp.getPort());
		return serverSocketChannel;
		
	    }
	}
