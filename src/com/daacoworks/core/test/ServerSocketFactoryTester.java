package com.daacoworks.core.test;

import com.daacoworks.core.factory.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.channels.ServerSocketChannel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServerSocketFactoryTester {
	SocketParameter socketParameter= new SocketParameter("localhost", 555);
	ServerSocketFactory serverSocketFactory= new ServerSocketFactory();
	
	@Before
	public void runBeforeTestMethod(){
		System.out.println("before");
	}
	
	/*@Test
	public void testCreateServer() throws IOException{
		ServerSocketChannel serverSocketChannel=serverSocketFactory.createServer(socketParameter);
		assertEquals(true,serverSocketChannel.isOpen());	
	}
	*/
	@Test
	public void testIsPortUsed(){
		boolean result;
		try{
		 serverSocketFactory.createServer(socketParameter);
		 result=true;
		}
		catch(Exception e)
		{
			System.out.println("Error:" +e);
			result=false;
		}
		 assertEquals(true,result);
	}
	
	
	@After
	public void runAfterTestMethod(){
		System.out.println("after"); 
	}
	

}
